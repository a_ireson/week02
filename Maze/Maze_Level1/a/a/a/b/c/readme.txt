

<><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

You did it!!!

             .-.
            /_ _\
            |o^o|
            \ _ /
           .-'-'-.
         /`)  .  (`\
        / /|.-'-.|\ \
        \ \| (_) |/ /  .-""-.
         \_\'-.-'/_/  /[] _ _\
         /_/ \_/ \_\ _|_o_LII|_
           |'._.'|  / | ==== | \
           |  |  |  |_| ==== |_|
            \_|_/    ||" ||  ||
            |-|-|    ||LI  o ||
            |_|_|    ||'----'||
           /_/ \_\  /__|    |__\

Impressive. Most impressive. But you are not a jedi yet. Now go to level 2 where you will need to use tab completion (or else you will go insane in the maze). Use pwd to see where you are. Try to jump back to the folder "Maze" in one step. Then go into Level 2.

<><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


