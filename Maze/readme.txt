Welcome to the commandline maze. The objective of this game is to get good at navigating through folders using the command line. You will need to use the following commands:

ls              ~ Lists the files and folders in the current folder.
pwd             ~ Tells you your current folder.
cd a            ~ Changes into the subfolder 'a'
cd a/b          ~ Changes into the subfolder 'b' that is inside the subfolder 'a'
cd ..           ~ Moves down one level
cd ../..        ~ Moves down two levels
cat readme.txt  ~ Prints the contents of the text file readme.txt to the screen

When you think you're ready to enter the maze, start by moving into the subfolder "Maze_Level1" and read the readme file in that folder.
