# Calculate the material costs of a widget, which is made from a number of parts.

PartName=['Fuse','LCD','Resistor','Button','Casing']
PartCost=[0.4,2.7,0.1,0.65,1.05]
NumberOfParts=[3,1,8,2,1]

n=len(PartName)
TotalCost=0.

for i in range(n):
    SubCost=NumberOfParts[i]*PartCost[i]
    TotalCost=TotalCost+SubCost
    print PartName[i]
    print SubCost

print "Total cost = %.2f" % (TotalCost)
